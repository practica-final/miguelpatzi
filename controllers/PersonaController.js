const { app, constants } = require('../config');
const { PersonaModel } = require('../models')
const axios = require('axios');

function respuesta(finalizado, mensaje , data) {
    return {
      finalizado,
      mensaje: mensaje,
      data
    }
  }

const registrarPersona = async (req,res)=> {
    try {
        console.log(req.body);
        const dato=req.body;
        const personaCreada = await PersonaModel.create(dato);
        res.status(200).json(respuesta(true,'Persona Creada',personaCreada));
    } catch (error) {
        res.status(400).json(respuesta(true,'Error al Crear Persona',error.message));
    }
} 


const listarPersonas = async (req, res) => {

    try {
        const listaPersonas = await PersonaModel.find();
        console.log(req.nombreCompleto);
        res.status(200).json(respuesta(true,'Persona Listado Corecctamente',listaPersonas));
          
    } catch (error) {
        res.status(400).json(respuesta(true,'Error al Listar',error.message));
    }
}



const buscarPersonaPorId = async (req,res) => {
    try {
        const { id } = req.params;
        const persona = await PersonaModel.findById(id);
        res.status(200).json(respuesta(true,'Persona encontrada',persona));

    } catch (error) {
        res.status(400).json(respuesta(false,'Error',error));
    }
  

};

const eliminarPersonaPorId = async (req,res) => {
    try {
        const { id } = req.params;
        const persona = await PersonaModel.findById(id);
        await PersonaModel.remove({ _id : id});
        res.status(200).json(respuesta(true,'Persona Eliminada',persona));

    } catch (error) {
        res.status(400).json(respuesta(false,'Error al Eliminar',error));
    }
  };

  const modificarPersonaPorId = async (req,res) => {
    try {
        const { id } = req.params;
        const dato = req.body;
        await PersonaModel.updateMany( { _id : id} ,{ $set: dato });
        res.status(200).json(respuesta(true,'Persona Modificada',dato));
        
    } catch (error) {
        res.status(400).json(respuesta(false,'No se pudo Modificar la Persona',error.message));
    }
   
  };


  const consumirServicio = async (req,res) => {
   const init = {
        method : 'GET',
        url : 'https://restcountries.eu/rest/v2/all'
    };
    const respuesta= await axios(init);
    res.status(200).json({
        finalizado : true,
        mensaje : 'Servicio Consumido',
        datos: respuesta.data
    });

}

const generarToken = (req,res) => {
    const { app } = require('../config')
    res.status(200).json( {
        finalizado : true,
        mensaje : ' Token Generado Correctamente',
        datos : `sdvdsdvsd5v12df1vd2fvdf2 ${app.expiracionToken}`
    })
}
module.exports = {
    listarPersonas,
    generarToken,
    registrarPersona,
    consumirServicio,
    buscarPersonaPorId,
    eliminarPersonaPorId,
    modificarPersonaPorId
}