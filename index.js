const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const { PersonaRouter } = require('./routers');
const { db } = require('./config');
// coneccion 
const mongoose = require('mongoose');
mongoose.connect(db.urlConteccion, {useNewUrlParser: true, useUnifiedTopology: true});

app.use(bodyParser.json());
app.use(PersonaRouter);


app.listen(PORT, () => {
 
  console.log(`Funcionando correctamente en el Puerto ${PORT}`);
 
})