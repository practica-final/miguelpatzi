const verificarToken =(req, res, next) => {
    try {
        const { authorization } = req.headers;
    console.log('==========DESDE MIDDLEWARE==================');
    console.log(authorization);
    if (authorization !== 'TOKEN') {
        throw new Error('No Autorizado');
    }
    req.nombreCompleto = 'Miguel Patzi';
    next(); 
    } catch (error) {
        res.status(401).json({
            finalizado : false,
            mesaje : error.message,
            datos : null
        });
    }
   
}

module.exports = { verificarToken }