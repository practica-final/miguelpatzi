const mongosse = require('mongoose');
const { Schema } =mongosse;

const PersonaSchema = new Schema({
    nombre : String,
    apellidoPaterno : String,
    apellidoMaterno : String,
    numeroDocumento : Number

});

const Persona = mongosse.model('persona',PersonaSchema);

module.exports = Persona;