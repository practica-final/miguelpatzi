const express = require('express');
const router = express.Router();

const { PersonaController } = require('../controllers');
const { AuthMiddleware } = require('../middleware')

router.post('/personas',PersonaController.registrarPersona);

router.get('/personas',  PersonaController.listarPersonas );

router.get('/personas/:id',  PersonaController.buscarPersonaPorId);

router.delete('/personas/:id',  PersonaController.eliminarPersonaPorId);

router.put('/personas/:id', PersonaController.modificarPersonaPorId);

router.get('/token', PersonaController.generarToken);

router.post('/paises',PersonaController.consumirServicio);

module.exports = router;